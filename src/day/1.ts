//2022 (after x-mas) by jramb

import * as readline from 'readline';

export async function solution(rl: readline.Interface) {
  let maximum = 0;
  let currentSum = 0;
  for await (const line of rl) {
    if (line == "") {
      maximum = Math.max(currentSum, maximum);
      currentSum = 0;
    } else {
      currentSum += +line;
    }
  }
  maximum = Math.max(currentSum, maximum);
  return maximum;
};


export async function solution2(rl: readline.Interface) {
  let maxima = [0, 0, 0];
  let currentSum = 0;
  function noteMaximum() {
    if (maxima[0] < currentSum) {
      maxima[0] = currentSum;
    }
    maxima.sort((a, b) => a - b);
  }
  for await (const line of rl) {
    if (line == "") {
      noteMaximum();
      currentSum = 0;
    } else {
      currentSum += +line;
    }
  }
  noteMaximum();
  let sumMaxima = 0;
  maxima.forEach((e) => sumMaxima += e);
  return sumMaxima;
};

