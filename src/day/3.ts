//2022 (after x-mas) by jramb
import * as readline from 'readline';

export async function solution(rl: readline.Interface) {
    let prioSum = 0;
    for await (const line of rl) {
        let c1 = line.substring(0, line.length / 2);
        let c2 = line.substring(line.length / 2);
        let re = new RegExp(`[^${c2}]`, 'g');
        let item = c1.replace(re, '')[0];

        prioSum += item.charCodeAt(0) - (item > 'Z' ? ('a'.charCodeAt(0) - 1) : ('A'.charCodeAt(0) - 27));
    }

    return prioSum;
};


export async function solution2(rl: readline.Interface) {
    let badgeSum = 0;
    let cnt3 = 0;
    let re: RegExp;
    let common = '';
    for await (const line of rl) {
        switch (cnt3) {
            case 0:
                common = line;
                break;
            case 1:
                re = new RegExp(`[^${line}]`, 'g');
                common = common.replace(re, '');
                break;
            case 2:
                re = new RegExp(`[^${line}]`, 'g');
                common = common.replace(re, '');
                let item = common[0];
                badgeSum += item.charCodeAt(0) - (item > 'Z' ? ('a'.charCodeAt(0) - 1) : ('A'.charCodeAt(0) - 27));
        }
        cnt3 = (cnt3 + 1) % 3;
    }
    return badgeSum;
};
