//2022 (after x-mas) by jramb
import * as readline from 'readline';
//import * as lo from 'lodash';

export async function solution(rl: readline.Interface) {
    let stacks: string[][] = [];
    let re = /move (\d+) from (\d+) to (\d+)/;
    for await (let line of rl) {
        if (line === "" || +line[1] > 0) {
            // nothing
        } else if (line[0] === "m") {
            let instr = line.match(re) as RegExpMatchArray;
            let cnt = +instr[1];
            let from = +instr[2] - 1;
            let to = +instr[3] - 1;
            if (!stacks[from]) stacks[from] = [];
            if (!stacks[to]) stacks[to] = [];
            while (cnt > 0) {
                let c = stacks[from].shift();
                if (c) {
                    stacks[to].unshift(c);
                }
                cnt--;
            }
            //console.log(stacks);
        } else {
            let stackNr = 0;
            while (line !== "") {
                let crate = line.substring(0, 3);
                if (crate[0] === '[') {
                    if (!stacks[stackNr]) stacks[stackNr] = [];
                    let crateId = crate[1];
                    stacks[stackNr].push(crateId);
                }
                stackNr++;
                line = line.substring(4);
            }
        }
    }
    return stacks.map((s) => s.shift()).join('');
};


export async function solution2(rl: readline.Interface) {
    let stacks: string[][] = [];
    let re = /move (\d+) from (\d+) to (\d+)/;
    for await (let line of rl) {
        if (line === "" || +line[1] > 0) {
            // nothing
        } else if (line[0] === "m") {
            let instr = line.match(re) as RegExpMatchArray;
            let cnt = +instr[1];
            let from = +instr[2] - 1;
            let to = +instr[3] - 1;
            let tmp: string[] = [];
            if (!stacks[from]) stacks[from] = [];
            if (!stacks[to]) stacks[to] = [];
            while (cnt > 0) {
                let c = stacks[from].shift();
                if (c) {
                    tmp.push(c);
                }
                cnt--;
            }
            let c:string;
            while (c = tmp.pop() as string) {
                stacks[to].unshift(c);
            }
        } else {
            let stackNr = 0;
            while (line !== "") {
                let crate = line.substring(0, 3);
                if (crate[0] === '[') {
                    if (!stacks[stackNr]) stacks[stackNr] = [];
                    let crateId = crate[1];
                    stacks[stackNr].push(crateId);
                }
                stackNr++;
                line = line.substring(4);
            }
        }
    }
    return stacks.map((s) => s.shift()).join('');
};