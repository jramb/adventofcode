//2022 (after x-mas) by jramb
import * as readline from 'readline';
import * as lo from 'lodash';

export async function solution(rl: readline.Interface) {
    let result = 0;
    let re = /(\d+)\-(\d+),(\d+)\-(\d+)/;
    for await (const line of rl) {
        let parsed = line.match(re);
        if (parsed) {
            let s1 = +parsed[1];
            let e1 = +parsed[2];
            let s2 = +parsed[3];
            let e2 = +parsed[4];
            if ((s1 >= s2 && e1 <= e2) || (s2 >= s1 && e2 <= e1))
                result++;
        }
    }
    return result;
};


export async function solution2(rl: readline.Interface) {
    let result = 0;
    let re = /(\d+)\-(\d+),(\d+)\-(\d+)/;
    for await (const line of rl) {
        let parsed = line.match(re);
        if (parsed) {
            var s1 = +parsed[1];
            var e1 = +parsed[2];
            var s2 = +parsed[3];
            var e2 = +parsed[4];
            if ((s1 >= s2 && s1 <= e2)
                || (s2 >= s1 && s2 <= e1)
            )
                result++;
        }
    }
    return result;
};