//2022 (after x-mas) by jramb

import * as readline from 'readline';

export async function solution(rl: readline.Interface) {
    let rpsHim = { 'A': 'r', 'B': 'p', 'C': 's' };
    let rpsMe = { 'X': 'r', 'Y': 'p', 'Z': 's' };
    let beats = { 'r': 's', 's': 'p', 'p': 'r' };
    let worth = { 'r': 1, 'p': 2, 's': 3 };
    let score = 0;
    for await (const line of rl) {
        let p = line.split(' ');
        let op = rpsHim[p[0] as 'A' | 'B' | 'C'] as 'r' | 'p' | 's';
        let me = rpsMe[p[1] as 'X' | 'Y' | 'Z' ] as 'r' | 'p' | 's';
        score += worth[me];
        score += (op === me) ? 3 : ((beats[me] === op) ? 6 : 0);
    }

    return score;
};


export async function solution2(rl: readline.Interface) {
    let rpsHim = { 'A': 'r', 'B': 'p', 'C': 's' };
    let beats = { 'r': 's' as 's', 's': 'p' as 'p', 'p': 'r' as 'r' };
    let worth = { 'r': 1, 'p': 2, 's': 3 };
    let score = 0;
    let me: 'r' | 'p' | 's' = 'r';
    for await (const line of rl) {
        let p = line.split(' ');
        let op = rpsHim[p[0] as 'A' | 'B' | 'C'] as 'r' | 'p' | 's';
        switch(p[1] as 'X' | 'Y' | 'Z' ) {
            case 'X': me = beats[op]; break;
            case 'Y': me = op; break;
            case 'Z': me = beats[beats[op]]; break;
        }
        score += worth[me];
        score += (op === me) ? 3 : ((beats[me] === op) ? 6 : 0);
    }

    return score;
};
