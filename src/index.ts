import * as readline from 'readline';
import * as fs from 'fs';



async function calcSolution(day: number, part: number, calcFunc: (rl: readline.Interface) => Promise<number|string>, expected?: number | string) {
    const inputFile = expected ? `./data/${day}-example.txt` : `./databig/${day}.txt`;
    const fileStream = fs.createReadStream(inputFile);
    const dayText = `Day ${day}${part === 1 ? 'a' : 'b'}` + (expected ? ' (test)' : '');

    const rl = readline.createInterface({
        input: fileStream,
        terminal: false,
        crlfDelay: Infinity //all instances of CR LF ('\r\n') in input.txt as a single line break.
    });

    let result = await calcFunc(rl);

    if (expected != null && result !== expected) {
        throw new Error(`${dayText}: result not expected: ${result} !== ${expected}`)
    }
    console.log(`${dayText}: ${result}`);
};


import * as d1 from "./day/1";
import * as d2 from "./day/2";
import * as d3 from "./day/3";
import * as d4 from "./day/4";
import * as d5 from "./day/5";

(async function () {
    try {
        await calcSolution(1, 1, d1.solution, 24000);
        await calcSolution(1, 1, d1.solution);
        await calcSolution(1, 2, d1.solution2, 45000);
        await calcSolution(1, 2, d1.solution2);
        await calcSolution(2, 1, d2.solution, 15);
        await calcSolution(2, 1, d2.solution);
        await calcSolution(2, 2, d2.solution2, 12);
        await calcSolution(2, 2, d2.solution2);
        await calcSolution(3, 1, d3.solution, 157);
        await calcSolution(3, 1, d3.solution);
        await calcSolution(3, 2, d3.solution2, 70);
        await calcSolution(3, 2, d3.solution2);
        await calcSolution(4, 1, d4.solution, 2);
        await calcSolution(4, 1, d4.solution);
        await calcSolution(4, 2, d4.solution2, 4);
        await calcSolution(4, 2, d4.solution2);
        await calcSolution(5, 1, d5.solution, 'CMZ');
        await calcSolution(5, 1, d5.solution);
        await calcSolution(5, 2, d5.solution2, 'MCD');
        await calcSolution(5, 2, d5.solution2);
    }
    catch (e) {
        console.log(e);
    }
})();

